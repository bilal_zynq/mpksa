$(document).ready(function () {
    $(".form_data").submit(function (e) {
        e.preventDefault();

        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                }
                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
    });


    if ($('.jFiler-input-text').length > 0) {

        $('.jFiler-input-text').hide();
    }


});

$(document).ready(function () {
    //$('.dataTables_length').children('label').children('select').addClass('selectpicker').data('style','select-with-transition');
});

// .dataTables_length

function showSuccess(message_text) {
    type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">' + message_text + '</span>'

    }, {
        type: type[2], // 2nd index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function showError(message_text) {
    type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">' + message_text + '</span>'

    }, {
        type: type[4],// 4th index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function deleteRecord(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    if (confirm(delete_msg)) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != false) {
                    showError(result.error);
                } else {
                    $('#' + id).remove();
                    showSuccess(result.success);
                }


                if (reloadUrl != "") setTimeout(function () {
                    document.location.href = reloadUrl;
                }, 1000);

            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}


function deleteImage(id, actionUrl) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to delete?")) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete_image'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {


                if (result.error != 'false') {
                    alert('There is something went wrong');

                } else {
                    $('#img-' + id).remove();
                    alert('Deleted Successfully');
                }


            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}


function redirect(redirect_to) {
    redirect_to = base_url + redirect_to;
    window.location.href = redirect_to;
}

$(".validate_ksa_number").blur(function () {
    var phone = $(this).val();
    if (phone.charAt(0) == '+' && phone.charAt(1) == '9' && phone.charAt(2) == '6' && phone.charAt(3) == '6' && phone.charAt(4) == '5' && phone.length == 13) {
        return true;
    } else {
        var name = $(this).attr('name');
        alert("You have entered an invalid phone number in " + name + " field !");
        return false;
    }
});

function assignOrder() {
    var technician_id = $('#technician').val();
    var vehicle_id = $('#vehicle').val();
    var booking_id = $('#BookingID').val();
    if (technician_id > 0 && vehicle_id > 0) {
        if (confirm('Are you sure to assign this order to this technician?')) {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });

            $.ajax({
                type: "POST",
                url: base_url + '' + 'cms/booking/update',
                data: {
                    'BookingID': booking_id,
                    'TechnicianID': technician_id,
                    'VehicleID': vehicle_id,
                    'Status': 'Assigned',
                    'SendAssignmentEmail': 'yes'
                },
                dataType: "json",
                cache: false,
                //async:false,
                success: function (result) {

                    if (result.error != false) {
                        showError(result.error);
                    } else {
                        showSuccess(result.success);
                        setTimeout(function () {
                            document.location.reload();
                        }, 2000);
                    }

                },
                complete: function () {
                    $.unblockUI();
                }
            });
            return true;
        }
    } else {
        showError('Please select technician and vehicle properly!');
    }
}