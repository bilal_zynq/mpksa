<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('Vehicle_model');
        $this->load->Model('City_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'VehicleID';
        $this->data['Table'] = 'vehicles';
    }

    public function index()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['results'] = $this->Vehicle_model->getLeftJoinedDataWithOtherTable(false, 'CityID', 'cities_text', 'cities_text.SystemLanguageID = 1');
        // dump($this->data['results']);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        if (!checkUserRightAccess(55, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['cities'] = $this->City_model->getAllCities($this->language, false);
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '')
    {
        if (!checkUserRightAccess(55, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['result'] = $this->Vehicle_model->getLeftJoinedDataWithOtherTable(false, 'CityID', 'cities_text', "vehicles.VehicleID = $id AND cities_text.SystemLanguageID = 1");
        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['cities'] = $this->City_model->getAllCities($this->language, false);
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data['VehicleID'] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('VehicleNumber', 'Vehicle Number', 'required|is_unique[' . $this->data['Table'] . '.VehicleNumber]');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {

        if (!checkUserRightAccess(55, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        $getSortValue = $this->Vehicle_model->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }

        $post_data['SortOrder'] = $sort;
        $post_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
        $post_data['CreatedAt'] = $post_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $post_data['CreatedBy'] = $post_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
        unset($post_data['form_type']);
        $insert_id = $this->Vehicle_model->save($post_data);
        if ($insert_id > 0) {
            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        $post_data = $this->input->post();
        if (isset($post_data['VehicleID'])) {
            $post_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);
            $post_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $post_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
            $VehicleID = $post_data['VehicleID'];
            unset($post_data['form_type']);
            unset($post_data['VehicleID']);
            // dump($post_data);
            unset($post_data['form_type']);
            $this->Vehicle_model->update($post_data, array('VehicleID' => $VehicleID));

            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $VehicleID;
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }


    private function delete()
    {
        if (!checkUserRightAccess(55, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

        $deleted_by['VehicleID'] = $this->input->post('id');
        $this->Vehicle_model->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');
        echo json_encode($success);
        exit;
    }


}