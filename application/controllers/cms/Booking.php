<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->Model('Booking_model');
        $this->load->Model('User_model');
        $this->load->Model('Vehicle_model');

        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['TableKey'] = 'BookingID';
        $this->data['Table'] = 'bookings';
    }

    public function index()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['results'] = $this->Booking_model->getBookings();
        $this->data['all'] = 1;
        // dump($this->data['results']);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function received()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "bookings.Status = 'Received'";
        $this->data['results'] = $this->Booking_model->getBookings($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function assigned()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "bookings.Status = 'Assigned'";
        $this->data['results'] = $this->Booking_model->getBookings($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function OnTheWay()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "bookings.Status = 'OnTheWay'";
        $this->data['results'] = $this->Booking_model->getBookings($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function reached()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "bookings.Status = 'Reached'";
        $this->data['results'] = $this->Booking_model->getBookings($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function completed()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "bookings.Status = 'Completed'";
        $this->data['results'] = $this->Booking_model->getBookings($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function cancelled()
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $where = "bookings.Status = 'Cancelled'";
        $this->data['results'] = $this->Booking_model->getBookings($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function view($id)
    {
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/view';
        $where = "bookings.BookingID = $id";
        $this->data['results'] = $this->Booking_model->getBookings($where);
        $this->data['technicians'] = $this->User_model->getUsers('users.RoleID = 2');
        $this->data['vehicles'] = $this->Vehicle_model->getLeftJoinedDataWithOtherTable(false, 'CityID', 'cities_text', "vehicles.IsActive = 1 AND cities_text.SystemLanguageID = 1");
        // dump($this->data['results']);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'delete':
                $this->delete();
                break;

        }
    }

    public function update()
    {
        $post_data = $this->input->post();
        if (isset($post_data['BookingID'])) {
            $BookingID = $post_data['BookingID'];
            unset($post_data['BookingID']);
            // dump($post_data);
            $this->Booking_model->update($post_data, array('BookingID' => $BookingID));
            if (isset($post_data['SendAssignmentEmail']) && $post_data['SendAssignmentEmail'] == 'yes')
            {
                $this->SendPushNotificationToTechnician();
                $this->SendSmsToTechnician($post_data['TechnicianID']);
                $this->SendEmailToTechnician();
            }
            // echo $this->db->last_query();exit();
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/view/' . $BookingID;
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }


    private function delete()
    {
        if (!checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->Booking_model->delete($deleted_by);

        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    private function SendPushNotificationToTechnician()
    {

    }

    private function SendSmsToTechnician($TechnicianID)
    {
        // send sms to technician
        $technician = $this->User_model->getUsers("users.UserID = $TechnicianID");
        if (isset($technician[0]->Mobile) && $technician[0]->Mobile != '')
        {
            sendSms($technician[0]->Mobile, 'An order is assigned to you by admin');
        }
    }

    private function SendEmailToTechnician()
    {

    }


}