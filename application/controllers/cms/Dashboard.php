<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Base_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
           
		parent::__construct();
		checkAdminSession();
                
                
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                
       
		
	}
	 
    
    public function index()
	{
          
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        
         
          $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    
	

}