<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

date_default_timezone_set('UTC');

class Api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->methods['user_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 5000; // 50 requests per hour per user/key
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        $this->load->model('City_model');
        $this->load->model('Package_model');
        $this->load->model('Page_model');

        $this->load->model('Booking_model');

        $this->load->model('Category_model');
        $this->load->model('Mobile_verification_model');

        $this->load->model('Site_setting_model');


    }


    public function generateTokenByApi_post()
    {
        $headers = $this->input->request_headers();
        if (isset($headers['Newtoken']) && $headers['Newtoken']) {
            if (isset($headers['Userid'])) {
                $id = $headers['Userid'];
            } else {
                $id = -1;
            }
            $payload = [
                'id' => $id,
                'other' => "Generate token"
            ];

            // Load Authorization Library or Load in autoload config file


            // generte a token
            $token = $this->authorization_token->generateToken($payload);
            $this->response([
                'status' => TRUE,
                'token' => $token
            ], REST_Controller::HTTP_OK);
        }
    }


    private function genrateToken($user_id)
    {
        $payload = [
            'id' => $user_id,
            'other' => ''
        ];

        // Load Authorization Library or Load in autoload config file


        // generte a token
        $token = $this->authorization_token->generateToken($payload);
        return $token;

    }


    private function _isUserAuthorized($not_check_for_user = false)
    {
        // Load Authorization Library
        //$this->load->library('authorization_token');
        $result = $this->authorization_token->validateToken();


        $post_data = $_REQUEST;
        if (isset($result['status']) AND $result['status'] === true) {

            if ($not_check_for_user) {
                return true;
            }


            if (isset($post_data['UserID']) && $post_data['UserID'] == $result['data']->id) {
                return true;
            } else {
                $this->response([
                    $this->config->item('rest_status_field_name') => FALSE,
                    $this->config->item('rest_message_field_name') => 'You don\'t have its access'
                ], self::HTTP_UNAUTHORIZED);

            }

            //return $result['data'];

        } else {

            if (isset($result['message'])) {
                $message = $result['message'];
            } else {
                $message = 'You don\'t have its access';
            }

            $this->response([
                $this->config->item('rest_status_field_name') => FALSE,
                $this->config->item('rest_message_field_name') => $message
            ], self::HTTP_UNAUTHORIZED);

            // $this->_response(['status' => FALSE, 'error' => $result['message']], self::HTTP_UNAUTHORIZED);
        }


        // check token is valid

    }


    public function test_get()
    {

        $this->_isUserAuthorized();

        echo 'herse';
        $post_data = $this->input->get('abc');
        dump($_REQUEST);
        $fields = $this->User_model->getFields();
        print_rm($fields);
        exit;
    }

    public function postTest_post()
    {
        $post_data = $this->input->get();
        dump($post_data);
    }

    public function testPusher_get()
    {
        $message = array('name' => 'bilal', 'message' => 'test message');
        pusher($message, true);
    }


    public function updateProfile_post()
    {

        $this->_isUserAuthorized();
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            if (isset($post_data['Email']) && $post_data['Email'] != '') {
                $user = $this->User_model->getWithMultipleFields(array('Email' => $post_data['Email']), true);
                if ($user) {
                    // sending back error
                    $this->response([
                        'status' => FALSE,
                        'message' => "Email address already exist"
                    ], REST_Controller::HTTP_OK);
                }
            }

            if (isset($post_data['Mobile'])) {
                $user = $this->User_model->getWithMultipleFields(array('Mobile' => $post_data['Mobile']), true);
                if ($user) {
                    // sending back error
                    $this->response([
                        'status' => FALSE,
                        'message' => "Mobile number already exist"
                    ], REST_Controller::HTTP_OK);
                }
            }


            $parent_tbl_data = $this->User_model->getFields();
            $child_tbl_data = $this->User_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data)) {
                    $save_parent_data[$key] = $value;


                } else if (in_array($key, $child_tbl_data)) {
                    $save_child_data[$key] = $value;
                }

            }

            $password = '';
            if (isset($save_parent_data['Password'])) {

                $password = $save_parent_data['Password'];
                $save_parent_data['Password'] = md5($save_parent_data['Password']);
            }

            $save_parent_data['UpdatedAt'] = time();


            if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {

                $file_name = $this->uploadImage("Image", "uploads/users/");
                $save_parent_data['Image'] = "uploads/users/" . $file_name;
            }
            if (isset($_FILES['CompressedImage']) && $_FILES['CompressedImage']['name'] != '') {

                $compressed_file_name = $this->uploadImage("CompressedImage", "uploads/users/compressed/");
                $save_parent_data['CompressedImage'] = "uploads/users/compressed/" . $compressed_file_name;
            }

            $update_by = array();
            $update_by['UserID'] = $post_data['UserID'];
            $this->User_model->update($save_parent_data, $update_by);
            if ($post_data['UserID'] > 0) {
                $update_by['SystemLanguageID'] = 1;
                $this->User_text_model->update($save_child_data, $update_by);
                $user_info = $this->User_model->getUserInfo('users.UserID =' . $post_data['UserID']);
                $user_info['AuthToken'] = $this->genrateToken($post_data['UserID']);
                $this->response([
                    'status' => TRUE,
                    'user_info' => NullToEmpty($user_info)
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Update profile failed. Please try again"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function categories_get()
    {

        $post_data = $this->input->get();
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }


        if (isset($post_data['Language'])) {
            $language = $post_data['Language'];
        } else {
            $language = 'EN';
        }
        if (isset($post_data['CategoryID'])) {
            $where = 'categories.ParentID =' . $post_data['CategoryID'] . ' AND system_languages.ShortCode = "' . $language . '"';
        } else {
            $where = 'categories.ParentID = 0 AND system_languages.ShortCode = "' . $language . '"';
        }

        $categories = $this->Category_model->getJoinedData(true, 'CategoryID', $where);

        $this->response([
            'status' => TRUE,
            'categories' => $categories
        ], REST_Controller::HTTP_OK);


    }


    public function packages_get()
    {

        $post_data = $this->input->get();
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }


        if (isset($post_data['Language'])) {
            $language = $post_data['Language'];
        } else {
            $language = 'EN';
        }
        $where = 'system_languages.ShortCode = "' . $language . '"';

        $packages = $this->Package_model->getJoinedData(true, 'PackageID', $where);

        $this->response([
            'status' => TRUE,
            'packages' => $packages
        ], REST_Controller::HTTP_OK);


    }


    public function sendOTP_post()
    {

        $this->_isUserAuthorized(true);
        $post_data = $this->input->post();
        if (!empty($post_data)) {

            $fetch_user = array();
            $fetch_user['Mobile'] = $post_data['Mobile'];
            $user_data = $this->User_model->getWithMultipleFields($fetch_user, true);
            $code = RandomString();
            $message = 'Your Verification Code For MPKSA Is ' . $code;
            $mobile_no = ltrim($post_data['Mobile'], '+');
            $mobile_no = str_replace(' ', '', $mobile_no);
            //$sms_sent = sendSms($mobile_no, $message);
            //if ($sms_sent) {
            if (true) {
                if ($user_data) {


                    $update_user = array();
                    $update_user_by = array();

                    $update_user['Code'] = $code;
                    $update_user_by['Mobile'] = $post_data['Mobile'];
                    $this->User_model->update($update_user, $update_user_by);
                    $this->response([
                        'status' => TRUE,
                        'message' => "Verification code sent",
                        'is_new_user' => 'no',
                        'otp_code' => $code
                    ], REST_Controller::HTTP_OK);


                } else {
                    $save_data = $get_data_by = array();
                    $save_data['Code'] = $code;
                    $save_data['Mobile'] = $get_data_by['Mobile'] = $post_data['Mobile'];
                    $mobile_verify = $this->Mobile_verification_model->getWithMultipleFields($get_data_by);
                    if ($mobile_verify) {

                        $update = array();
                        $update_by = array();

                        $update['Code'] = $code;
                        $update_by['Mobile'] = $post_data['Mobile'];

                        $this->Mobile_verification_model->update($update, $update_by);

                    } else {
                        $save_data['CreatedAt'] = date('Y-m-d H:i:s');
                        $insert_id = $this->Mobile_verification_model->save($save_data);
                    }


                    $this->response([
                        'status' => TRUE,
                        'message' => "Verification code sent",
                        'is_new_user' => 'yes',
                        'otp_code' => $code
                    ], REST_Controller::HTTP_OK);

                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Verification code not sent. Please make sure have you inserted valid mobile number."
                ], REST_Controller::HTTP_OK);

            }


        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }


    }


    public function getUserDetail_get()
    {
        $this->_isUserAuthorized();
        $post_data = $this->input->get();
        $user_info = array();
        if (!empty($post_data)) {
            $result = $this->User_model->getUserInfo('users.UserID =' . $post_data['UserID']);

            if (!empty($result)) {
                $user_info = $result;

            }
            $this->response([
                'status' => TRUE,
                'user_info' => $user_info
            ], REST_Controller::HTTP_OK);

        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getPageDetail_get()
    {


        $post_data = $this->input->get();
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $page_info = array();
        $page_info_arr = array();
        if (!empty($post_data)) {
            if (isset($post_data['Language'])) {
                $language = $post_data['Language'];
            } else {
                $language = 'EN';
            }


            if (isset($post_data['Multiple']) && $post_data['Multiple'] == TRUE) {
                $page_id = " AND pages.PageID IN (" . $post_data['PageID'] . ")";
            } else {
                $page_id = " AND pages.PageID = " . $post_data['PageID'];
            }

            $result = $this->Page_model->getJoinedData(true, 'PageID', 'system_languages.ShortCode = "' . $language . '"' . $page_id . '');
            $site_settings = $this->Site_setting_model->get(1, false, 'SiteSettingID');
            //echo $this->db->last_query();exit;

            if (!empty($result)) {
                $page_info = $result;
                // dump($page_info);
                if (isset($post_data['Multiple']) && $post_data['Multiple'] == TRUE) {
                    // there are 0,1 indexes in arrays when using Multiple true so removing them by loop
                    foreach ($page_info as $key => $value) {
                        $page_info_arr[$value['PageID']] = $value;
                        //$page_info_arr[$value['PageID']]['Description'] = str_replace(array("\n\r", "\n", "\r"), '', $value['Description']);
                    }
                    $page_info = $page_info_arr;
                } else {
                    $page_info = $page_info[0];
                    //$page_info['Description'] = str_replace(array("\n\r", "\n", "\r"), '', $page_info['Description']);
                }
            }

            $this->response([
                'status' => TRUE,
                'page_data' => $page_info,
                'open_time' => $site_settings->OpenTime,
                'close_time' => $site_settings->CloseTime
            ], REST_Controller::HTTP_OK);

        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function saveBooking_post()
    {


        $post_data = $this->input->post();
        $this->_isUserAuthorized(true);
        $booking_info = array();
        if (!empty($post_data)) {
            if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {
                $file_name = $this->uploadImage("Image", "uploads/bookings/");
                $post_data['Image'] = "uploads/bookings/" . $file_name;
            }

            $insert_id = $this->Booking_model->save($post_data);
            if ($insert_id > 0) {

                $update_by = array();
                $update['OrderNumber'] = $insert_id . RandomString();
                $update_by['BookingID'] = $insert_id;
                $this->Booking_model->update($update, $update_by);
                $result = $this->Booking_model->getBookings('bookings.BookingID = ' . $insert_id);
                if (!empty($result)) {
                    $booking_info = $result;
                }
                $this->response([
                    'status' => TRUE,
                    'message' => "Your booking is placed successfully",
                    'booking_info' => $booking_info
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Booking not done. Something went wrong"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function getUserBookings_get()
    {
        $post_data = $this->input->get();
        $this->_isUserAuthorized();
        $booking_info = array();
        if (!empty($post_data)) {
            $where = 'bookings.UserID = ' . $post_data['UserID'];
            if (isset($post_data['Status']) && $post_data['Status'] != '') {
                if ($post_data['Status'] == 'pending')
                {
                    $where .= " AND (bookings.Status = 'Received' OR bookings.Status = 'Assigned' OR bookings.Status = 'OnTheWay' OR bookings.Status = 'Reached')";
                } elseif ($post_data['Status'] == 'approved')
                {
                    $where .= " AND bookings.Status = 'Completed'";
                } elseif ($post_data['Status'] == 'cancel')
                {
                    $where .= " AND bookings.Status = 'Cancelled'";
                }
            }
            $result = $this->Booking_model->getBookings($where);
            //echo $this->db->last_query();exit;
            if (!empty($result)) {
                $booking_info = $result;
            }

            $this->response([
                'status' => TRUE,
                'booking_info' => $booking_info
            ], REST_Controller::HTTP_OK);

        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getBookingDetail_get()
    {


        $post_data = $this->input->get();
        $this->_isUserAuthorized();
        $page_info = array();
        if (!empty($post_data)) {


            $result = $this->Booking_model->getBookings('bookings.BookingID = ' . $post_data['BookingID']);
            //echo $this->db->last_query();exit;
            if (!empty($result)) {
                $booking_info = $result;
            }


            $this->response([
                'status' => TRUE,
                'booking_info' => $booking_info
            ], REST_Controller::HTTP_OK);

        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function verifyOTP_post()
    {
        $this->_isUserAuthorized(true);
        $post_data = $this->input->post();
        if (!empty($post_data)) {

            $fetch_user = array();
            $fetch_user['Mobile'] = $post_data['Mobile'];
            $user_data = $this->User_model->getWithMultipleFields($fetch_user, true);
            if ($user_data) {

                if ($user_data['Code'] == $post_data['Code']) {

                    $update_user = array();
                    $update_user_by = array();

                    $update_user['Code'] = '';
                    $update_user_by['Mobile'] = $post_data['Mobile'];
                    $this->User_model->update($update_user, $update_user_by);
                    $user_info = array();
                    $user_info = $this->User_model->getUserInfo('users.UserID =' . $user_data['UserID']);


                    $user_info['AuthToken'] = $this->genrateToken($user_data['UserID']);


                    $this->response([
                        'status' => TRUE,
                        'message' => "Code verified",
                        'user_info' => NullToEmpty($user_info)
                    ], REST_Controller::HTTP_OK);

                } else {


                    $this->response([
                        'status' => FALSE,
                        'message' => "Verification code is incorrect"
                    ], REST_Controller::HTTP_OK);

                }


            } else {

                $get_data_by = array();
                $get_data_by['Code'] = $post_data['Code'];
                $get_data_by['Mobile'] = $post_data['Mobile'];
                $mobile_verify = $this->Mobile_verification_model->getWithMultipleFields($get_data_by);

                if ($mobile_verify) {
                    $this->Mobile_verification_model->delete($get_data_by);

                    $save_user = array();
                    $save_user_text = array();

                    $save_user['Mobile'] = $post_data['Mobile'];
                    $save_user['IsActive'] = 1;
                    $save_user['RoleID'] = 3;
                    $save_user['CreatedAt'] = $save_user['UpdatedAt'] = date('Y-m-d H:i:s');

                    $user_id = $this->User_model->save($save_user);

                    if ($user_id > 0) {
                        $save_user_text = array();
                        $save_user_text['UserID'] = $user_id;
                        $save_user_text['FullName'] = '';
                        $save_user_text['SystemLanguageID'] = 1;//default eng

                        $this->User_text_model->save($save_user_text);
                        $user_info = array();
                        $user_info = $this->User_model->getUserInfo('users.UserID =' . $user_id);

                        $user_info['AuthToken'] = $this->genrateToken($user_id);


                        $this->response([
                            'status' => TRUE,
                            'message' => "Code verified",
                            'user_info' => NullToEmpty($user_info)
                        ], REST_Controller::HTTP_OK);


                    } else {
                        $this->response([
                            'status' => FALSE,
                            'message' => "Code is verified but data is not save to database.Something went wrong"
                        ], REST_Controller::HTTP_OK);

                    }


                } else {

                    $this->response([
                        'status' => FALSE,
                        'message' => "Verification code is not correct"
                    ], REST_Controller::HTTP_OK);

                }


            }


        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function cities_get()
    {
        if (isset($post_data['UserID'])) {
            $this->_isUserAuthorized();
        } else {
            $this->_isUserAuthorized(true);
        }
        $post_data = $_REQUEST;
        if (isset($post_data['Language'])) {
            $language = $post_data['Language'];
        } else {
            $language = 'EN';
        }
        $where = 'cities.IsActive = 1 AND system_languages.ShortCode = "' . $language . '"';

        $cities = $this->City_model->getJoinedData(true, 'CityID', $where);

        $this->response([
            'status' => TRUE,
            'cities' => $cities
        ], REST_Controller::HTTP_OK);


    }


    private function sendEmail($txt)
    {
        $to = "bilal.e@zynq.net";
        $subject = "MSJ Test";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <no-reply@zynq.net>' . "\r\n";
        mail($to, $subject, $txt, $headers);
    }

    private function sendWelcomeEmail($user_id, $password)
    {
        $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $user_id . '');
        $user = $user[0];
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hobbies</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}

#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>

<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi ' . $user['FullName'] . ' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Thank you for registering with Hobbies. We hope to provide you the best we can.</p>
<p style="font-family:sans-serif;font-size:14px;">Your login details are:</p>
<p style="font-family:sans-serif;font-size:14px;">Email: ' . $user['Email'] . '</p>
<p style="font-family:sans-serif;font-size:14px;">Password: ' . $password . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="' . base_url() . '" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Hobbies</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">hobbies.schopfen.com</a>
</span>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>
';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <no-reply@hobbies.com.sa>' . "\r\n";
        mail($user['Email'], 'Welcome at Hobbies', $message, $headers);

    }

    private function sendForgotPasswordEmail($user, $password)
    {
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <no-reply@hobbies.schopfen.com>' . "\r\n";

        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hobbies</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}

#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>

<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi ' . $user->FullName . ' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Forgot Your Password ?</p>
<p style="font-family:sans-serif;font-size:14px;">No worries , we have a new one for you. Please use following password to login.</p>
<p style="font-family:sans-serif;font-size:14px;font-weight: bold;">Password: ' . $password . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="' . base_url() . '" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Hobbies</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">hobbies.schopfen.com</a>
</span>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>
';

        mail($user->Email, 'Forgot Password', $message, $headers);
        return true;
    }

    private function sendThankyouEmail()
    {
        /*$data['content'] = '<h4>Hi Bilal Ejaz,</h4>
                                                <p>Thank you for Purchasing from MSJ Security Systems.</p>
                                                <p> Your order # 123123123 has been placed successfully.</p>';
        $message = $this->load->view('front/emails/general_email', $data, true);
        echo $message;exit();*/
        $email_data['body'] = "Dear Bilal Ejaz , Your account is suspended at MSJ. Please contact admin for details and further action.";
        $email_body = emailTemplate($email_data['body']);
        echo $email_body;
        exit();
        $order_id = $this->input->get('order_id');
        $order_details = $this->Model_order->getDetail($order_id);
        $data['order_details'] = $order_details;
        $message = $this->load->view('front/emails/order_confirmation', $data, true);
        echo $message;
        exit();
        $email_data['to'] = 'bilal.e@zynq.net';
        $email_data['subject'] = 'Order received at MSJ : Order # ' . $order_details[0]['order_track_id'];
        $email_data['from'] = 'noreply@msj.com.sa';
        $email_data['body'] = $message;
        sendEmail($email_data);
        return true;
    }

    private function uploadImage($key, $path)
    {
        $file_name = rand(999, 999999) . date('Ymdhsi') . $_FILES[$key]['name'];
        $file_size = $_FILES[$key]['size'];
        $file_tmp = $_FILES[$key]['tmp_name'];
        $file_type = $_FILES[$key]['type'];
        move_uploaded_file($file_tmp, $path . $file_name);
        return $file_name;
    }

}

