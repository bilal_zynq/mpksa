<?php
if (!file_exists($result[0]->Image)) {
    $image = base_url("assets/backend/img/no_img.png");
} else {
    $image = base_url($result[0]->Image);
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add'); ?> Technician</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="UserID" value="<?php echo $UserID; ?>">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FullName">Full Name</label>
                                        <input type="text" name="FullName" required class="form-control" id="FullName"
                                               value="<?php echo $result[0]->FullName; ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Email">Email</label>
                                        <input type="text" name="Email" required class="form-control" id="Email"
                                               value="<?php echo $result[0]->Email; ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Mobile">Mobile No.</label>
                                        <input type="text" name="Mobile" required class="form-control" id="Mobile"
                                               value="<?php echo $result[0]->Mobile; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CityID">City</label>
                                        <select class="selectpicker" id="CityID" data-style="select-with-transition"
                                                data-live-search="true" required name="CityID">
                                            <?php
                                            foreach ($cities as $city) {
                                                if ($result[0]->CityID == $city->CityID) {
                                                    $selected = 'selected';
                                                } else {
                                                    $selected = '';
                                                }
                                                ?>
                                                <option value="<?php echo $city->CityID; ?>" <?php echo $selected; ?>><?php echo $city->Title; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CompanyName">Company Name</label>
                                        <input type="text" name="CompanyName" required class="form-control"
                                               id="CompanyName" value="<?php echo $result[0]->CompanyName; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="<?php echo $image; ?>" target="_blank">
                                        <img src="<?php echo $image; ?>" style="height: 100px; width: 100px;">
                                    </a>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group"><label>Image</label>
                                        <input type="file" name="Image[]" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"
                                                       checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit'); ?>
                                </button>
                                <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back'); ?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>