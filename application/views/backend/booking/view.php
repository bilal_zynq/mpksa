<?php
$status = $results[0]['Status'];
if ($status == 'Received') {
    $class = "btn btn-sm";
} else if ($status == 'Assigned') {
    $class = "btn btn-primary btn-sm";
} else if ($status == 'OnTheWay') {
    $class = "btn btn-warning btn-sm";
} else if ($status == 'Reached') {
    $class = "btn btn-info btn-sm";
} else if ($status == 'Completed') {
    $class = "btn btn-success btn-sm";
} else if ($status == 'Cancelled') {
    $class = "btn btn-danger btn-sm";
}
?>
<div class="content">
    <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">
                                Details For Booking # <b><?php echo $results[0]['OrderNumber']; ?></b>
                                &nbsp;&nbsp;<button class="<?php echo $class; ?>"><?php echo $status; ?>
                                    <div class="ripple-container"></div>
                                </button>
                            </h5>
                        </div>
                        <div class="card-content">
                            <div class="row clearfix">
                                <div class="col-sm-4">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Service Title</label>
                                            <h5><?php echo $results[0]['CategoryTitle']; ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Service Date</label>
                                            <h5><?php echo $results[0]['BookingDate'] != '' ? getFormattedDateTime($results[0]['BookingDate'], 'l\, F jS\, Y') : 'N/A'; ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Service Time</label>
                                            <h5><?php echo $results[0]['BookingTime'] != '' ? getFormattedDateTime($results[0]['BookingTime'], 'h:i A') : 'N/A'; ?></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Amount</label>
                                            <h5><?php echo $results[0]['Amount']; ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Address</label>
                                            <h5><?php echo ($results[0]['Address'] != '' ? $results[0]['Address'] : 'N/A'); ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">See location on map</label>
                                            <a href="https://www.google.com/maps/search/?api=1&query=<?php echo $results[0]['AddressLat']; ?>,<?php echo $results[0]['AddressLong']; ?>" target="_blank">
                                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHbVVHxTASKeTMj0H-JFtHN138p-i6Rx-UdC0VQ2l17yJcaRFVCQ" height="25" width="25" style="height: 40px !important;width: 40px !important;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Image</label>
                                            <?php
                                            if ($results[0]['Image'] != '')
                                            { ?>
                                                <a href="<?php echo base_url().$results[0]['Image']; ?>" target="_blank" title="Click to view in new tab"><img src="<?php echo base_url().$results[0]['Image']; ?>" style="width: 160px !important;height: 160px !important;"></a>
                                            <?php } else { ?>
                                                <h5>N/A</h5>
                                            <?php }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Booking Description</label>
                                            <h5><?php echo ($results[0]['Description'] != '' ? $results[0]['Description'] : 'N/A'); ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Coupon Used</label>
                                            <h5><?php echo ($results[0]['Coupon'] != '' ? $results[0]['Coupon'] : 'N/A'); ?></h5>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Assign Order </h5>
                        </div>
                        <div class="card-content">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label" for="technician">Technician</label>
                                            <select class="selectpicker" id="technician" data-style="select-with-transition" data-live-search="true" required name="technician">
                                                <option value="" disabled selected>Please select technician</option>
                                                <?php
                                                foreach ($technicians as $technician)
                                                {
                                                    if ($technician->UserID == $results[0]['TechnicianID'])
                                                    {
                                                        $selected_technician = 'selected';
                                                    } else {
                                                        $selected_technician = '';
                                                    }
                                                    ?>
                                                    <option value="<?php echo $technician->UserID; ?>" <?php echo $selected_technician; ?>>
                                                        <?php echo $technician->FullName; ?> (<?php echo $technician->CityTitle; ?>)
                                                    </option>
                                                <?php }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label" for="vehicle">Vehicle</label>
                                            <select class="selectpicker" id="vehicle" data-style="select-with-transition" data-live-search="true" required name="vehicle">
                                                <option value="" disabled selected>Please select vehicle</option>
                                                <?php
                                                foreach ($vehicles as $vehicle)
                                                {
                                                    if ($vehicle->VehicleID == $results[0]['VehicleID'])
                                                    {
                                                        $selected_vehicle = 'selected';
                                                    } else {
                                                        $selected_vehicle = '';
                                                    }
                                                    ?>
                                                    <option value="<?php echo $vehicle->VehicleID; ?>" <?php echo $selected_vehicle; ?>>
                                                        <?php echo $vehicle->VehicleNumber; ?> (<?php echo $vehicle->Title; ?>)
                                                    </option>
                                                <?php }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="BookingID" value="<?php echo $results[0]['BookingID']; ?>">
                                <div class="col-sm-12">
                                    <div class="form-group label-floating">
                                        <button class="btn btn-primary waves-effect waves-light pull-right" type="button" onclick="assignOrder();">
                                            <?php echo lang('submit');?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Customer Details </h5>
                        </div>
                        <div class="card-content">
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Customer Name</label>
                                            <h5>
                                                <a href="<?php echo base_url('cms/user/edit') . '/' . $results[0]['UserID']; ?>"
                                                   target="_blank"><?php echo $results[0]['FullName']; ?></a></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Mobile No.</label>
                                            <h5><?php echo($results[0]['Mobile'] != '' ? '<a href="tel:' . $results[0]['Mobile'] . '">' . $results[0]['Mobile'] . '</a>' : 'N/A'); ?></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Email</label>
                                            <h5><?php echo($results[0]['Email'] != '' ? '<a href="mailto:' . $results[0]['Email'] . '">' . $results[0]['Email'] . '</a>' : 'N/A'); ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">User Type</label>
                                            <h5><?php echo($results[0]['UserType'] != '' ? ucfirst($results[0]['UserType']) : 'N/A'); ?></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">User City</label>
                                            <h5><?php echo($results[0]['UserCity'] != '' ? ucfirst($results[0]['UserCity']) : 'N/A'); ?></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Assigned Technician & Vehicle Details </h5>
                        </div>
                        <div class="card-content">
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Technician Name</label>
                                            <h5>
                                                    <?php echo ($results[0]['TechnicianFullName'] != '' ? '<a href="'.base_url('cms/technician/edit') . '/' . $results[0]['TechnicianID'].'" target="_blank">'.$results[0]['TechnicianFullName'].'</a>' : 'N/A'); ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Technician Mobile No.</label>
                                            <h5><?php echo($results[0]['TechnicianMobile'] != '' ? '<a href="tel:' . $results[0]['TechnicianMobile'] . '">' . $results[0]['TechnicianMobile'] . '</a>' : 'N/A'); ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Technician Email</label>
                                            <h5><?php echo($results[0]['TechnicianEmail'] != '' ? '<a href="mailto:' . $results[0]['TechnicianEmail'] . '">' . $results[0]['TechnicianEmail'] . '</a>' : 'N/A'); ?></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label">Vehicle Number</label>
                                            <h5>
                                                <?php echo ($results[0]['VehicleNumber'] != '' ? '<a href="'.base_url('cms/vehicle/edit') . '/' . $results[0]['VehicleID'].'" target="_blank">'.$results[0]['VehicleNumber'].'</a>' : 'N/A'); ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="col-lg-4 col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Booking Status </h5>
                        </div>
                        <div class="card-content">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group label-floating">
                                        <div class="form-line">
                                            <label class="control-label" for="technician">Technician</label>
                                            <select class="selectpicker" id="technician" data-style="select-with-transition" data-live-search="true" required name="technician">
                                                <option value="" disabled selected>Please booking status</option>
                                                <option value="Received" <?php /*echo ($results[0]['Status'] == 'Received' ? 'selected' : ''); */?>>Received</option>
                                                <option value="Assigned" <?php /*echo ($results[0]['Status'] == 'Assigned' ? 'selected' : ''); */?>>Assigned</option>
                                                <option value="OnTheWay" <?php /*echo ($results[0]['Status'] == 'OnTheWay' ? 'selected' : ''); */?>>On The Way</option>
                                                <option value="Reached" <?php /*echo ($results[0]['Status'] == 'Reached' ? 'selected' : ''); */?>>Reached</option>
                                                <option value="Completed" <?php /*echo ($results[0]['Status'] == 'Completed' ? 'selected' : ''); */?>>Completed</option>
                                                <option value="Cancelled" <?php /*echo ($results[0]['Status'] == 'Cancelled' ? 'selected' : ''); */?>>Cancelled</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="BookingID" value="<?php /*echo $results[0]['BookingID']; */?>">
                                <div class="col-sm-12">
                                    <div class="form-group label-floating">
                                        <button class="btn btn-primary waves-effect waves-light pull-right" type="button" onclick="assignOrder();">
                                            <?php /*echo lang('submit');*/?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
    </div>
</div>