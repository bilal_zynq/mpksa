<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo ucfirst($this->uri->segment(3)) . ' ' . lang($ControllerName . 's'); ?></h4>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Booking #</th>
                                    <th>Service Title</th>
                                    <th>Customer Name</th>
                                    <th>Customer Mobile #</th>
                                    <th>Customer Email</th>
                                    <th>Service Date</th>
                                    <th>Service Time</th>
                                    <?php if (isset($all)) { ?>
                                        <th>Status</th>
                                    <?php } ?>
                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                        <th><?php echo lang('actions'); ?></th>
                                    <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($results) {
                                    foreach ($results as $value) { ?>
                                        <tr id="<?php echo $value['BookingID']; ?>">
                                            <td><?php echo $value['OrderNumber']; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/category/edit') . '/' . $value['CategoryID']; ?>"
                                                   target="_blank"><?php echo $value['CategoryTitle']; ?></a></td>
                                            <td>
                                                <a href="<?php echo base_url('cms/user/edit') . '/' . $value['UserID']; ?>"
                                                   target="_blank"><?php echo $value['FullName']; ?></a></td>
                                            <td><?php echo($value['Mobile'] != '' ? '<a href="tel:' . $value['Mobile'] . '">' . $value['Mobile'] . '</a>' : 'N/A'); ?></td>
                                            <td><?php echo($value['Email'] != '' ? '<a href="mailto:' . $value['Email'] . '">' . $value['Email'] . '</a>' : 'N/A'); ?></td>
                                            <td><?php echo $value['BookingDate'] != '' ? getFormattedDateTime($value['BookingDate'], 'l\, F jS\, Y') : 'N/A'; ?></td>
                                            <td><?php echo $value['BookingTime'] != '' ? getFormattedDateTime($value['BookingTime'], 'h:i A') : 'N/A'; ?></td>
                                            <?php if (isset($all)) {
                                                $status = $value['Status'];
                                                if ($status == 'Received') {
                                                    $class = "btn btn-sm";
                                                } else if ($status == 'Assigned') {
                                                    $class = "btn btn-primary btn-sm";
                                                } else if ($status == 'OnTheWay') {
                                                    $class = "btn btn-warning btn-sm";
                                                } else if ($status == 'Reached') {
                                                    $class = "btn btn-info btn-sm";
                                                } else if ($status == 'Completed') {
                                                    $class = "btn btn-success btn-sm";
                                                } else if ($status == 'Cancelled') {
                                                    $class = "btn btn-danger btn-sm";
                                                }
                                                ?>
                                                <td>
                                                    <button class="<?php echo $class; ?>"><?php echo $status; ?>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </td>
                                            <?php } ?>
                                            <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit') || checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                <td>
                                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                        <a href="<?php echo base_url('cms/' . $ControllerName . '/view/' . $value['BookingID']); ?>"
                                                           class="btn btn-simple btn-warning btn-icon edit"><i
                                                                    class="material-icons"
                                                                    title="Click to view details">assignment</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>

                                                    <?php if (checkUserRightAccess(46, $this->session->userdata['admin']['UserID'], 'CanDelete')) { ?>
                                                        <a href="javascript:void(0);"
                                                           onclick="deleteRecord('<?php echo $value['BookingID']; ?>','cms/<?php echo $ControllerName; ?>/action','')"
                                                           class="btn btn-simple btn-danger btn-icon remove"><i
                                                                    class="material-icons" title="Delete">close</i>
                                                            <div class="ripple-container"></div>
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>