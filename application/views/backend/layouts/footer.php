<footer class="footer">
    <div class="container-fluid">
        <p class="copyright pull-right">
            <a href="<?php echo base_url('cms/dashboard'); ?>"><?php echo $site_setting->SiteName; ?></a> | &copy;
            Niehez 6 <?php echo date('Y'); ?>
        </p>
    </div>
</footer>
</div>
</div>
</body>


<!-- Forms Validations Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo base_url(); ?>assets/backend/js/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.21/moment-timezone-with-data.js"></script>
<!--  Charts Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!-- Select Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?php echo base_url(); ?>assets/backend/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.tagsinput.js"></script>
<script src="<?php echo base_url();?>assets/backend/plugins/summernote/summernote.min.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo base_url(); ?>assets/backend/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url(); ?>assets/backend/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/script.js?v=<?php echo rand(); ?>"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.blockUI.js"></script>
<!--
<script src="<?php echo base_url();?>assets/backend/plugins/jquery.filer/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/jquery.fileuploads.init.js"></script>-->

<?php if ($this->session->flashdata('message')) { ?>
    <script>

        $(document).ready(function () {
            showError('<?php echo $this->session->flashdata('message'); ?>');
        });

    </script>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function () {
        /*$('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });*/

        $(".summernote").summernote({
            styleWithSpan: true,
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });

        $('.inline-editor').summernote({
            airMode: true
        });

        $('.custom_datepicker').datetimepicker({format: 'DD-MM-YYYY'});
    });

    setTimeout(function(){ $( ".summernote-arb" ).css( "direction", "rtl" );  }, 1000);
</script>

<script>
    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    function eraseCookie(name) {
        document.cookie = name + '=; Max-Age=-99999999;';
    }

    var base_url = "<?php echo base_url() ?>";
    var system_timezone = moment.tz.guess();
    eraseCookie('system_timezone');
    setCookie('system_timezone', system_timezone, 7);
    console.log("Local system timezone is: "+system_timezone);
</script>

</html>
