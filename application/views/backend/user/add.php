<?php
$option = '';
if(!empty($districts)){
    foreach($districts as $district){
        $option .= '<option value="'.$district->DistrictID.'" '.((isset($result[0]->DistrictID) && $result[0]->DistrictID == $district->DistrictID) ? 'selected' : '').'>'.$district->Title.' </option>';
    } }


?>
<style type="text/css">
    .app_user_field{
        display: none;
    }
</style>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">



                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="RoleID"><?php echo lang('choose_user_role'); ?> *</label>
                                        <select id="RoleID" class="selectpicker" data-style="select-with-transition" required name="RoleID">

                                            <?php if(!empty($roles)){
                                                foreach($roles as $role){ ?>
                                                    <option value="<?php echo $role->RoleID; ?>"><?php echo $role->Title; ?> </option>
                                                <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('name'); ?></label>
                                        <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title">
                                    </div>
                                </div>
                            </div>



                            <div class="row">


                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Email"><?php echo lang('email'); ?></label>
                                        <input type="text" name="Email" parsley-trigger="change" required  class="form-control" id="Email">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Password"><?php echo lang('password'); ?><?php echo lang('min_length'); ?></label>
                                        <input type="password" name="Password" parsley-trigger="change" required  class="form-control" id="Password">
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ConfirmPassword"><?php echo lang('confirm_password'); ?></label>
                                        <input type="password" name="ConfirmPassword" parsley-trigger="change" required  class="form-control" id="ConfirmPassword">
                                    </div>
                                </div>
                                 <div class="col-sm-6 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>



                            <div class="row app_user_field">
                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="UserType">User Type *</label>
                                        <select id="UserType" class="selectpicker" data-style="select-with-transition" required name="UserType">
                                        <option value="individual">Individual</option>

                                         <option value="company">Individual</option>                                                
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Mobile</label>
                                        <input type="text" name="Mobile" parsley-trigger="change" required  class="form-control" id="Mobile">
                                    </div>
                                </div>
                            </div>
                            <div class="row app_user_field">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CityID">cities *</label>
                                        <select id="CityID" class="selectpicker" data-style="select-with-transition" required name="CityID">

                                            <?php if(!empty($cities)){
                                                foreach($cities as $city){ ?>
                                                    <option value="<?php echo $city->CityID; ?>"><?php echo $city->Title; ?> </option>
                                                <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<script type="text/javascript">
    
   $(document).ready(function () {
        $("#RoleID").on('change',function(){
            if($(this).val() == 3){
                $('.app_user_field').show();
            }else{
               $('.app_user_field').hide(); 
            }
        });
   });
</script>