<style type="text/css">
    .app_user_field{
        display: none;
    }
</style>
<?php
$option = '';
if(!empty($roles)){ 
    foreach($roles as $role){ 
        $option .= '<option value="'.$role->RoleID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } }
$option2 = '';
if(!empty($cities)){ 
    foreach($cities as $city){ 
        $option2 .= '<option value="'.$city->CityID.'" '.((isset($result[0]->CityID) && $result[0]->CityID == $city->CityID) ? 'selected' : '').'>'.$city->Title.' </option>';
    } }     
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        if($key == 0){
         $common_fields = '<div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="RoleID'.$key.'">'.lang('choose_user_role').'</label>
                                                                <select id="RoleID'.$key.'" class="selectpicker" data-style="select-with-transition" required name="RoleID">
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div>';
        $common_fields2 = ' <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Email">'.lang('email').'</label>
                                                                <input type="text" disabled name="Email" parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div>'; 
        $common_fields3 = '<div class="row">
                               '.$common_fields2.'
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label class="control-label" for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>';
            $common_fields4 = '<div class="row '.((isset($result[$key]->RoleID) && $result[$key]->RoleID == 3) ? '' : 'app_user_field').'">
                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="UserType">User Type *</label>
                                        <select id="UserType" class="selectpicker" data-style="select-with-transition" required name="UserType">
                                        <option value="individual" '.((isset($result[$key]->UserType) && $result[$key]->UserType == 'individual') ? 'selected' : '').'>Individual</option>

                                         <option value="company" '.((isset($result[$key]->UserType) && $result[$key]->UserType == 'company') ? 'selected' : '').'>Company</option>                                                
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Mobile</label>
                                        <input type="text" name="Mobile" parsley-trigger="change" required  class="form-control" id="Mobile" value="'.((isset($result[0]->Mobile)) ? $result[0]->Mobile : '').'" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row '.((isset($result[$key]->RoleID) && $result[$key]->RoleID == 3) ? '' : 'app_user_field').'">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CityID">cities *</label>
                                        <select id="CityID" class="selectpicker" data-style="select-with-transition" required name="CityID">

                                            '.$option2.'
                                        </select>
                                    </div>
                                </div>
                            </div>';                
       
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                      <div class="row">
                                                     '.$common_fields.'
                                                     <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('name').'</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->FullName)) ? $result[$key]->FullName : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                      
                                                    
                                                   '.$common_fields3.'

                                                   '.$common_fields4.'
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Languages</h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
   $(document).ready(function () {
        $("#RoleID").on('change',function(){
            if($(this).val() == 3){
                $('.app_user_field').show();
            }else{
               $('.app_user_field').hide(); 
            }
        });
   });
</script>