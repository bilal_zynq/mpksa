<?php

Class User_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("users");

    }

    public function getUserInfo($where, $system_language_code = 'EN')
    {

        $this->db->select('users.*,users_text.FullName,cities_text.Title as CityTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('cities_text', 'cities_text.CityID = users.CityID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        
        $this->db->where($where);
        
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        return $this->db->get()->row_array();


    }

    public function getUserData($data, $system_language_code = false)
    {

        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('roles', 'roles.RoleID = users.RoleID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
        // $this->db->where($where);
        $this->db->where('users.Email', $data['Email']);
        //$this->db->or_where('users.UName',$data['Email']);
        $this->db->where('users.Password', $data['Password']);

        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        return $this->db->get()->row_array();


    }

    public function getUsers($where = false, $system_language_code = 'EN')
    {

        $this->db->select('users.*,users_text.FullName,cities_text.Title as CityTitle,roles_text.Title as RoleTitle');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('roles_text', 'users.RoleID = roles_text.RoleID');
        $this->db->join('cities_text', 'cities_text.CityID = users.CityID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        if($where){
            $this->db->where($where);
        }
        
        
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }


        $this->db->group_by('users.UserID');
        return $this->db->get()->result();


    }
    
  

}

?>