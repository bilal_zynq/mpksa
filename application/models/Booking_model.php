<?php
Class Booking_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("bookings");

    }


    public function getBookings($where = false){

    	$this->db->select('bookings.*,users.Email,users.Mobile,users_text.FullName,categories_text.Title as CategoryTitle,categories.Image as CategoryImage,users.UserType, cities_text.Title as UserCity, tst.FullName as TechnicianFullName, ts.Email as TechnicianEmail, ts.Mobile as TechnicianMobile, v.VehicleNumber as VehicleNumber');
    	$this->db->from('bookings');

    	$this->db->join('users','bookings.UserID = users.UserID');
    	$this->db->join('users_text','users.UserID = users_text.UserID');

    	// technician details
        $this->db->join('users ts','bookings.TechnicianID = ts.UserID', 'LEFT');
        $this->db->join('users_text tst','ts.UserID = tst.UserID', 'LEFT');

        // vehicle details
        $this->db->join('vehicles v','bookings.VehicleID = v.VehicleID', 'LEFT');

    	$this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID','Left' );

        $this->db->join('cities','users.CityID = cities.CityID','left');
        $this->db->join('cities_text','cities.CityID = cities_text.CityID','left');

    	$this->db->join('categories','bookings.CategoryID = categories.CategoryID','left');
    	$this->db->join('categories_text','categories.CategoryID = categories_text.CategoryID','left');

    	$this->db->where('system_languages.IsDefault', '1');
    	if($where){
    		$this->db->where($where);
    	}

    	$this->db->group_by('bookings.BookingID');
    	$this->db->order_by('bookings.BookingID','DESC');


    	return $this->db->get()->result_array();




    }


}