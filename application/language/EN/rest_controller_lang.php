<?php

/*
 * English language
 */

//General 

$lang['some_thing_went_wrong'] = 'There is something went wrong';


$lang['save_successfully']     = 'Save Successfully';
$lang['update_successfully']   = 'Updated Successfully';
$lang['deleted_successfully']  = 'Deleted Successfully';
$lang['you_should_update_data_seperately_for_each_language']  = 'You sould need to update data separately for each language';
$lang['yes']  = 'Yes';
$lang['no']  = 'No';
$lang['actions']  = 'Action';
$lang['submit']  = 'Submit';
$lang['back']  = 'Back';
$lang['add']  = 'Add';
$lang['edit']  = 'Edit';
$lang['are_you_sure']  = 'Are you sure you want to delete this?';
$lang['view']     = 'View';
$lang['add']     = 'Add';
$lang['edit']     = 'Edit';
$lang['delete']     = 'Delete';
$lang['is_active']                 = 'Is Active';
$lang['create_table']                 = 'Create Table';
$lang['create_model']                 = 'Create Model';
$lang['create_view']                 = 'Create View';
$lang['create_controller']                 = 'Create Controller';
$lang['logout']                 = 'Logout';

$lang['please_add_role_first'] = 'Please add role first';
$lang['you_dont_have_its_access'] = 'You don\'t have its access';

$lang['SiteName'] = 'Site Name';
$lang['PhoneNumber'] = 'Phone Number';
$lang['Whatsapp'] = 'Whatsapp';
$lang['Skype'] = 'Skype';
$lang['Fax'] = 'Fax';
$lang['SiteLogo'] = 'Site Logo';
$lang['EditSiteSettings'] = 'Edit Site Settings';
$lang['FacebookUrl'] = 'Facebook Url';
$lang['GoogleUrl'] = 'Google Url';
$lang['LinkedInUrl'] = 'LinkedIn Url';
$lang['TwitterUrl'] = 'Twitter Url';
//end General

// Module Section
$lang['choose_parent_module'] = 'Choose Parent Module';
$lang['parent_module']        = 'Parent Module';
$lang['slug']                 = 'Slug';
$lang['icon_class']            = 'Icon Class';

$lang['title']                 = 'Title';
$lang['add_module']            = 'Add Module';
$lang['modules']                = 'Modules';
$lang['module']                = 'Module';
$lang['module_rights']                = 'Module Rights';

$lang['please_add_module_first']                = 'Please add module first';
$lang['rights']            = 'Rights';


// Roles Section

$lang['select_role']            = 'Select Role';
$lang['add_role']               = 'Add Role';
$lang['roles']                  = 'Roles';
$lang['role']                   = 'Role';
$lang['please_add_role_first']                = 'Please add role first';
$lang['choose_user_role'] = 'Choose user role';



// user

$lang['please_add_user_first'] = 'Please add user first';

$lang['name']                    = 'Name';
$lang['user']                    = 'User';
$lang['users']                    = 'Users';
$lang['add_user']                    = 'Add User';
$lang['select_user']                    = 'Select User';

$lang['email']                    = 'Email';
$lang['password']                 = 'Password';
$lang['min_length']               = '(Min char 8)';
$lang['confirm_password']         = 'Confirm Password';





$lang['add_city']            = 'Add City';
$lang['citys']            = 'Citys';
$lang['city']            = 'City';
$lang['add_district']            = 'Add District';
$lang['districts']            = 'Districts';
$lang['district']            = 'District';
$lang['please_add_distric_first'] = 'Please add distric first';
$lang['choose_district'] = 'Choose District';
$lang['add_email_template'] = 'Add Email Template';
$lang['email_templates'] = 'Email Templates';
$lang['email_template'] = 'Email Template';
$lang['Heading'] = 'Heading';
$lang['Description'] = 'Description';
$lang['Image'] = 'Image';
$lang['add_language']            = 'Add Language';
$lang['languages']            = 'Languages';
$lang['language']            = 'Language';
$lang['short_code']            = 'Short Code';

$lang['is_default']            = 'Is Default';
$lang['please_select_default_value_language_first'] = 'Please select another default value language first';
$lang['add_category']            = 'Add Category';
$lang['categorys']            = 'Categories';
$lang['category']            = 'Category';
$lang['add_child_category']            = 'Add Child Category';
$lang['child_catagories_of'] = 'Child Categories Of';
$lang['price']            = 'Price';

$lang['add_package']            = 'Add Package';
$lang['packages']            = 'Packages';
$lang['package']            = 'Package';

$lang['visit_per_year']            = 'Visit Per Year';
$lang['add_page']            = 'Add Page';
$lang['pages']            = 'Pages';
$lang['page']            = 'Page';
$lang['add_booking']            = 'Add Booking';
$lang['bookings']            = 'Bookings';
$lang['booking']            = 'Booking';
$lang['add_booking/OnTheWay']            = 'Add Booking/OnTheWay';
$lang['booking/OnTheWays']            = 'Booking/OnTheWays';
$lang['booking/OnTheWay']            = 'Booking/OnTheWay';
$lang['add_vehicle']            = 'Add Vehicle';
$lang['vehicles']            = 'Vehicles';
$lang['vehicle']            = 'Vehicle';