<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


function currentDate()
{
    return date('Y-m-d H:i:s');
}


function print_rm($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}

function dump($data)
{
    echo '<pre>';
    print_r($data);
    exit;
}


function checkRightAccess($module_id, $role_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Module_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['RoleID'] = $role_id;
    $fetch_by[$can] = 1;

    $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}


function NullToEmpty($data)
{
    $returnArr = array();
    if (isset($data[0])) // checking if array is a multi-dimensional one, if so then checking for each row
    {
        $i = 0;
        foreach ($data as $row) {
            if (is_object($row)) {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i]->$key = "";
                    } else {
                        $returnArr[$i]->$key = $value;
                    }
                }
            } else {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i][$key] = "";
                    } else {
                        $returnArr[$i][$key] = $value;
                    }
                }
            }
            $i++;
        }
    } else {
        if (is_object($data)) {
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr->$key = "";
                } else {
                    $returnArr->$key = $value;
                }
            }
        } else {
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr[$key] = "";
                } else {
                    $returnArr[$key] = $value;
                }
            }
        }
    }
    return $returnArr;
}

function checkUserRightAccess($module_id, $user_id, $can)
{
    $CI = &get_Instance();
    $CI->load->model('Modules_users_rights_model');

    $fetch_by = array();
    $fetch_by['ModuleID'] = $module_id;
    $fetch_by['UserID'] = $user_id;
    $fetch_by[$can] = 1;

    $result = $CI->Modules_users_rights_model->getWithMultipleFields($fetch_by);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

function getSystemLanguages()
{
    $CI = &get_Instance();
    $CI->load->model('System_language_model');
    $languages = $CI->System_language_model->getAllLanguages();
    return $languages;
}

function getDefaultLanguage()
{
    $CI = &get_Instance();
    $CI->load->Model('System_language_model');
    $fetch_by = array();
    $fetch_by['IsDefault'] = 1;
    $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
    return $result;
}


function getAllActiveModules($role_id, $system_language_id, $where)
{
    $CI = &get_Instance();
    $CI->load->Model('Module_rights_model');
    $result = $CI->Module_rights_model->getModulesWithRights($role_id, $system_language_id, $where);
    return $result;
}


function getActiveUserModule($user_id, $system_language_id, $where)
{
    $CI = &get_Instance();
    $CI->load->Model('Modules_users_rights_model');
    $result = $CI->Modules_users_rights_model->getModulesWithRights($user_id, $system_language_id, $where);
    return $result;
}

function checkAdminSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('admin')) {
        return true;

    } else {
        redirect($CI->config->item('base_url').'cms');
    }
}


function sendEmail($data = array())
{
    $CI = &get_Instance();
    $CI->load->library('email');
    $CI->email->from($data['from']);
    $CI->email->to($data['to']);
    $CI->email->subject($data['subject']);
    $CI->email->message($data['body']);
    $CI->email->set_mailtype('html');
    if ($CI->email->send()) {
        return true;
    } else {
        return false;
    }
}

function sendSmsOfficial() // This is official function kept here so we can use it anywhere we want. It has all the functionality unifonic supports
{
    require FCPATH . '/vendor/Unifonic/Autoload.php';
    $client = new \Unifonic\API\Client();
    try {
        $response = $client->Messages->Send('+923323911622', 'test unifonic for bilal', 'abcd'); // send regular massage
        dump($response);
        //$response = $client->Account->GetBalance();
        //$response = $client->Account->getSenderIDStatus('Arabic');
        //$response = $client->Account->getSenderIDs();
        //$response = $client->Account->GetAppDefaultSenderID();
        //$response = $client->Messages->Send('recipient','messageBody','senderName');
        //$response = $client->Messages->SendBulkMessages('96650*******,9665*******','Hello','UNIFONIC');
        //$response = $client->Messages->GetMessageIDStatus('9459*******');
        //$response = $client->Messages->GetMessagesReport();
        //$response = $client->Messages->GetMessagesDetails();
        //$response = $client->Messages->GetScheduled();
        echo '<pre>';
        print_r($response);
    } catch (Exception $e) {
        echo $e->getCode();
    }
}

function sendSms($mobile_no, $msg, $debug = false) // Provide mobile no with country code, AppsID is configured in vendor/Unifonic/config.php
{
    require FCPATH . '/vendor/Unifonic/Autoload.php';
    $client = new \Unifonic\API\Client();
    try {
        $response = $client->Messages->Send($mobile_no, $msg, 'MP-App');
        if ($debug) // If this is true and message sent in try block then it will dump response here
        {
            dump($response);
        }
        if (isset($response->Status) && ($response->Status == 'Queued' || $response->Status == 'Sent' || $response->Status == 'Delivered')) {
            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        $error = $e->getCode();
        if ($debug) // If this is true and message failed to sent in try block then it will echo error message here
        {
            echo $error;
        }
        return false;
    }
}


function RandomString($digit = 4)
{
    $characters = '123456789123456789123456789123456789123456789';
    $randstring = '';
    for ($i = 0; $i < $digit; $i++) {
        $randstring .= $characters[rand(0, 40)];
    }
    return $randstring;
}

function generatePIN($digits = 4){
    $i = 0; //counter
    $pin = ""; //our default pin is blank.
    while($i < $digits){
        //generate a random number between 0 and 9.
        $pin .= mt_rand(0, 9);
        $i++;
    }
    return $pin;
}

function log_notification($data)
{
    $CI = &get_Instance();
    $CI->load->model('User_notification_model');
    $result = $CI->User_notification_model->save($data);
    if ($result > 0) {
        return true;
    } else {
        return false;
    }
}

function sendPushNotificationToAndroid($title, $message, $registatoin_ids, $data = array())
{
    $android_fcm_key = 'AIzaSyD6ZGRJ_X2N1yQa9J40LhX8KISQetlMtrc';

    $sendData['title'] = $title;
    $sendData['body'] = $message;
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array(
        "registration_ids" => $registatoin_ids,
        "content_available" => true,
        "priority" => "high",
        "notification" => array
        (
            "title" => $title,
            "body" => $message,
            "sound" => "default"
        ),
        "data" => array
        (
            "body" => $message,
            "notificationKey" => $registatoin_ids,
            "priority" => "high",
            "sound" => "default",
            "notification_data" => $data
        ),
    );

    $headers = array(
        'Authorization:key=' . $android_fcm_key,
        'Content-Type: application/json'
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);

    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

function sendPushNotificationToIOS($title, $message, $register_keys, $data = array())
{
    $ios_fcm_key = 'AIzaSyDC8tK7DBt7vyBG7F8tRbUDJwiSgbZfZOg';

    $fields = array(
        //"to" => $gcm_ios_mobile_reg_key,
        "registration_ids" => $register_keys, //1000 per request logic is pending
        "content_available" => true,
        "priority" => "high",
        "notification" => array(
            "body" => strip_tags($message),
            "title" => $title,
            "sound" => "default"
        ),
        "data" => array
        (
            "body" => $message,
            "notificationKey" => $register_keys,
            "priority" => "high",
            "sound" => "default",
            "notification_data" => $data
        ),
    );


    $url = 'https://gcm-http.googleapis.com/gcm/send'; //note: its different than android.


    $headers = array(
        'Authorization: key=' . $ios_fcm_key,
        'Content-Type: application/json'
    );


    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        // echo 'abc';
        die('Curl failed: ' . curl_error($ch));
    }
    // echo 'cdf';
    //print_r($result);exit();
    // Close connection
    curl_close($ch);
    return $result;
}

function sendNotification($title, $message, $data, $user_id)
{
    $CI = &get_Instance();
    $CI->load->model('User_model');
    $res = 'Device token not found!';
    $user = $CI->User_model->get($user_id, true, 'UserID');
    if ($user['DeviceToken'] != '') {
        $token = array($user['DeviceToken']);
        if (strtolower($user['DeviceType']) == 'android') {
            $res = sendPushNotificationToAndroid($title, $message, $token, $data);
        } elseif (strtolower($user['DeviceType']) == 'ios') {
            $res = sendPushNotificationToIOS($title, $message, $token, $data);
        }
    }
    return $res;
}

function pusher($data, $type, $channel, $event, $debug = false)
{
    require FCPATH . '/vendor/autoload.php';

    $options = array(
        'cluster' => 'eu',
        'encrypted' => true
    );

    //$pusher = new Pusher\Pusher(
    // $app_key,
    //$app_secret,
    // $app_id,
    //array('cluster' => $app_cluster) );

    $pusher = new Pusher\Pusher(
        '52b110a5299ef34a0bff',
        'd48b6cf0c2cfcfb732f4',
        '640393',
        $options
    );

    $channel = 'Hobbies_Channel_' . $channel;
    $event = 'Hobbies_Event_' . $event;
    $data['type'] = $type;

    $response = $pusher->trigger($channel, $event, $data);
    if ($debug) {
        echo '<pre>';
        print_r($response);
        exit();
    }
    return true;
}

function convertTimestampToLocalDatetime($timestamp)
{
    // first converting timestamp to GMT date time
    $datetime = gmdate("Y-m-d H:i:s", $timestamp);
    // getting local timezone
    $current_timezone = $_COOKIE['system_timezone'];

    // creating date time object from the date time coming in UTC format
    $utc_date = DateTime::createFromFormat(
        'Y-m-d H:i:s',
        $datetime,
        new DateTimeZone('UTC')
    );
    $acst_date = clone $utc_date;

    // setting timezone to local timezone for UTC time coming above
    $acst_date->setTimeZone(new DateTimeZone($current_timezone));

    // formatting datetime
    $original_time = $utc_date->format('Y-m-d H:i:s');
    $converted_local_time = $acst_date->format('Y-m-d H:i:s');
    return $converted_local_time;
}

function getFormattedDateTime($timestamp, $format)
{
    return date($format, strtotime(convertTimestampToLocalDatetime($timestamp)));
}